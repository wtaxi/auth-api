module gitlab.com/wtaxi/auth-api

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/djumanoff/amqp v1.0.5
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/go-uuid v1.0.2
	github.com/lib/pq v1.8.0
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	googlemaps.github.io/maps v1.2.3
)
