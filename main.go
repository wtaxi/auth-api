package main

import (
	"context"
	"gitlab.com/wtaxi/auth-api/auth"
	"gitlab.com/wtaxi/auth-api/helper"
	"log"
	"net/http"
)

var (
	// postgres env
	postgresHost                = "database-central.c9vdzz9niagi.eu-central-1.rds.amazonaws.com"
	postgresUser                = "postgres"
	postgresPassword            = "Newpass12"
	postgresDatabase            = "wtaxi"
	postgresPort                = 5432
	// redis env
	redisHost                   = "3.120.152.71"
	redisPort                   = 6379
	redisPassword               = "Newpass12"
	redisDB                     = 1
	// firebase env
	firebaseNotificationUrl     = "https://fcm.googleapis.com/fcm/send"
	firebaseNotificationAuthKey = "AAAAjM4Ho98:APA91bGMWTbgBp485cO6dPq3OQLYuwYEJjJ7EZz43-kUGsR85IFUIlyP57PAOWW_iSVenYsPyWNnQ3fClq3wrr8f-SZLjEmVk0euHKyq5cC6LKW6BNGlM9IxTmR309VF6u90cGS0K223"
)

func main() {
	postgresConf := auth.PostgresConfig{
		Host:     postgresHost,
		Port:     postgresPort,
		User:     postgresUser,
		Password: postgresPassword,
		Database: postgresDatabase,
		Params:   "sslmode=disable",
	}

	mapsCtx := context.Background()
	redisConf := auth.RedisConfig{
		Host:     redisHost,
		Password: redisPassword,
		Port:     redisPort,
		DB:       redisDB,
	}

	httpClt := http.Client{}
	serviceConfig := &auth.ServiceConfig{
		UserStoreConfig: postgresConf,
		RedisConfig:     redisConf,
		HttpClt:         httpClt,
		SMSSenderCreds: auth.SmsSenderCredentials{
			Login:    "root4",
			Password: "Mynewpass12",
		},
		MapsServiceConfig: auth.MapsConfig{
			Ctx:      mapsCtx,
			ApiToken: "AIzaSyBmGQS3wdM_xIJwPFxmjNMbZV9xY9OfKKY",
		},
		CostPer100Meter:             5,
		FirebaseNotificationURL:     firebaseNotificationUrl,
		FirebaseNotificationAuthKey: firebaseNotificationAuthKey,
	}
	service, err := auth.NewAuthService(serviceConfig)
	if err != nil {
		panic(err)
	}
	httpFac := auth.NewHttpEndpointFactory(service)
	r := helper.NewCORSRouter()

	//r.Handle("/sign_up", helper.Handler{H: httpFac.SignUpEndpoint}).Methods("POST")
	r.Handle("/sign_up", helper.NewHandler(httpFac.SignUpEndpoint)).Methods("GET", "POST", "PUT")
	r.Handle("/token", helper.NewHandler(httpFac.TokenEndpoint)).Methods("POST")
	r.Handle("/welcome", helper.NewHandlerWithACL(httpFac.WelcomeEndpoint)).Methods("GET")
	r.Handle("/direction", helper.NewHandlerWithACL(httpFac.GetDirectionEndpoint)).Methods("GET")
	r.Handle("/address_info", helper.NewHandler(httpFac.GetAddressInfoEndpoint)).Methods("GET")
	r.Handle("/psg/order", helper.NewHandlerWithACL(httpFac.OrderTaxiEndpoint)).Methods("POST")
	r.Handle("/drv/order", helper.NewHandlerWithACL(httpFac.DriverOrderEndpoint)).Methods("POST", "GET")
	r.Handle("/phone/update", helper.NewHandlerWithACL(httpFac.UpdatePhoneEndpoint)).Methods("GET", "POST")
	r.Handle("/password/update", helper.NewHandlerWithACL(httpFac.UpdatePasswordEndpoint)).Methods("POST")
	r.Handle("/password/reset", helper.NewHandler(httpFac.ResetPasswordEndpoint)).Methods("GET", "POST", "PUT")
	r.HandleFunc("/drv/order/updates", httpFac.DriverConnectionEndpoint)
	r.HandleFunc("/psg/order/updates", httpFac.PassengerConnectionEndpoint)

	r.Handle("/drv/order/ready", helper.NewHandlerWithACL(httpFac.ReadyToStartEndpoint)).Methods("POST")
	r.Handle("/drv/order/start", helper.NewHandlerWithACL(httpFac.StartRideEndpoint)).Methods("POST")
	r.Handle("/drv/order/end", helper.NewHandlerWithACL(httpFac.EndRideEndpoint)).Methods("POST")

	log.Println("Server started...")
	http.Handle("/", r)
	// Logs the error if ListenAndServe fails.
	log.Fatal(http.ListenAndServe(":8000", nil))
}
