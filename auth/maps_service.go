package auth

import (
	"context"
	"googlemaps.github.io/maps"
)

type MapsService struct {
	clt *maps.Client
	ctx context.Context
}

type MapsConfig struct {
	Ctx      context.Context
	ApiToken string
}

func NewMapsService(cfg MapsConfig) (*MapsService, error) {
	c, err := maps.NewClient(maps.WithAPIKey(cfg.ApiToken))
	if err != nil {
		return nil, err
	}
	return &MapsService{
		clt: c,
		ctx: cfg.Ctx,
	}, nil
}

func (svc *MapsService) GetDirection(directionReq *maps.DirectionsRequest) ([]maps.Route, []maps.GeocodedWaypoint, error) {
	return svc.clt.Directions(svc.ctx, directionReq)
}

func (svc *MapsService) GetAddressInfo(geocodingReq *maps.GeocodingRequest) ([]maps.GeocodingResult, error) {
	geocodingReq.Language = "ru"
	return svc.clt.Geocode(svc.ctx, geocodingReq)
}

func (svc *MapsService) IsNear(origin, destination string) error {
	routes, _, err := svc.clt.Directions(svc.ctx, &maps.DirectionsRequest{
		Origin:                   origin,
		Destination:              destination,
		Mode:        "driving",
	})
	if err != nil {
		return err
	}
	if routes[0].Legs[0].Distance.Meters > 100 {
		return NewError(403, "too far")
	}
	return nil
}