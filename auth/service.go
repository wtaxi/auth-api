package auth

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/hashicorp/go-uuid"
	"gitlab.com/wtaxi/auth-api/helper"
	"golang.org/x/crypto/bcrypt"
	"googlemaps.github.io/maps"
	"log"
	"math/rand"
	"net/http"
	"time"
)

type Service struct {
	userStore                   Store
	verificationStore           VerificationStore
	smsSenderService            SMSSenderService
	mapsService                 MapsService
	orderStore                  OrderStore
	costPer100Meter             float64
	firebaseNotificationService FirebaseNotificationService
}

type ServiceConfig struct {
	UserStoreConfig             PostgresConfig
	RedisConfig                 RedisConfig
	HttpClt                     http.Client
	SMSSenderCreds              SmsSenderCredentials
	MapsServiceConfig           MapsConfig
	CostPer100Meter             float64
	FirebaseNotificationURL     string
	FirebaseNotificationAuthKey string
}

func NewAuthService(cfg *ServiceConfig) (*Service, error) {
	mapsService, err := NewMapsService(cfg.MapsServiceConfig)
	if err != nil {
		return nil, err
	}
	verificationStore, err := NewVerificationStore(cfg.RedisConfig)
	if err != nil {
		return nil, err
	}
	orderStore, err := NewOrderStore(cfg.RedisConfig)
	if err != nil {
		return nil, err
	}
	userStore, err := NewUserStore(cfg.UserStoreConfig)
	if err != nil {
		return nil, err
	}
	smsService := NewSmsService(cfg.HttpClt, cfg.SMSSenderCreds.Login, cfg.SMSSenderCreds.Password)
	firebaseNotificationService := FirebaseNotificationService{
		Clt:         cfg.HttpClt,
		AuthKey:     cfg.FirebaseNotificationAuthKey,
		FirebaseURL: cfg.FirebaseNotificationURL,
	}
	return &Service{
		userStore:                   userStore,
		verificationStore:           *verificationStore,
		smsSenderService:            smsService,
		mapsService:                 *mapsService,
		costPer100Meter:             cfg.CostPer100Meter,
		orderStore:                  *orderStore,
		firebaseNotificationService: firebaseNotificationService,
	}, nil
}

func (svc *Service) SignUpSend(phone string) (*PhoneVerifyResponse, error) {
	if err := isPhoneFormat(phone); err != nil {
		return nil, err
	}

	existingUser, _ := svc.userStore.GetByPhone(phone)
	if existingUser != nil {
		return nil, NewError(403, "user with phone: "+phone+" already exist")
	}

	code := codeGenerator(6)
	token, err := uuid.GenerateUUID()
	if err != nil {
		return nil, err
	}
	err = svc.verificationStore.Set(&PhoneVerification{
		PhoneNumber: phone,
		Code:        code,
		Token:       token,
		Status:      "sign_up_send",
		TTL:         3 * time.Minute,
	})
	if err != nil {
		return nil, err
	}

	// TODO uncomment
	//err = svc.smsSenderService.Send(&SMS{
	//	Phone:   phone,
	//	Message: code,
	//})
	//if err != nil {
	//	return nil, err
	//}
	return &PhoneVerifyResponse{VerificationToken: token}, nil
}

func (svc *Service) SignUpVerify(token, code string) (*PhoneVerifyResponse, error) {
	item, err := svc.verificationStore.Get(token)
	if err != nil {
		return nil, err
	}
	if item.Status != "sign_up_send" {
		return nil, ErrInvalidVerificationToken
	}

	// TODO uncomment
	//if item.Code != code {
	//	return nil, ErrInvalidCode
	//}
	if "000000" != code {
		return nil, ErrInvalidCode
	}

	err = svc.verificationStore.Set(&PhoneVerification{
		PhoneNumber: item.PhoneNumber,
		Code:        item.Code,
		Token:       item.Token,
		Status:      "sign_up_verified",
		TTL:         3 * time.Minute,
	})
	if err != nil {
		return nil, err
	}

	return &PhoneVerifyResponse{token}, nil
}

func (svc *Service) Token(req *TokenRequest) (*TokenResponse, error) {
	switch req.GrantType {
	case "refresh":
		return svc.RefreshToken(req.Claims)
	case "password":
		return svc.SignIn(req)
	default:
		return nil, NewError(405, "unknown grant type")
	}
}

func (svc *Service) SignUp(token, password string) (*TokenResponse, error) {
	item, err := svc.verificationStore.Get(token)
	if err != nil {
		return nil, err
	}
	if item.Status != "sign_up_verified" {
		return nil, ErrInvalidVerificationToken
	}

	user := &User{
		PhoneNumber:         item.PhoneNumber,
		PhoneNumberVerified: true,
	}
	newPass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, Error{500, err}
	}
	user.Password = string(newPass)

	user, err = svc.userStore.Create(user)
	if err != nil {
		return nil, err
	}

	err = svc.verificationStore.Delete(token)
	if err != nil {
		return nil, err
	}

	tkn, err := helper.GenerateToken(&helper.Claims{
		PhoneNumber: user.PhoneNumber,
		StandardClaims: jwt.StandardClaims{
			Subject: user.ID,
		},
	})
	return &TokenResponse{tkn}, nil
}

func (svc *Service) SignIn(cmd *TokenRequest) (*TokenResponse, error) {
	if cmd.PhoneNumber == "" {
		return nil, NewError(400, "phone number is empty")
	}
	if cmd.Password == "" {
		return nil, NewError(400, "password is empty")
	}
	user, err := svc.userStore.GetByPhone(cmd.PhoneNumber)
	if err != nil {
		return nil, err
	}
	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(cmd.Password)); err != nil {
		return nil, Error{401, err}
	}

	tkn, err := helper.GenerateToken(&helper.Claims{
		PhoneNumber: user.PhoneNumber,
		StandardClaims: jwt.StandardClaims{
			Subject: user.ID,
		},
	})
	return &TokenResponse{tkn}, nil
}

func (svc *Service) RefreshToken(claims *helper.Claims) (*TokenResponse, error) {
	tkn, err := helper.GenerateToken(claims)
	if err != nil {
		return nil, err
	}
	return &TokenResponse{Token: tkn}, nil
}

func (svc *Service) UpdatePassword(req *UpdatePasswordRequest) (*ResponseOK, error) {
	newPass, err := bcrypt.GenerateFromPassword([]byte(req.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		return nil, Error{500, err}
	}
	pass := string(newPass)

	_, err = svc.userStore.Update(&UserUpdate{ID: req.UserID, Password: &pass})
	if err != nil {
		return nil, err
	}

	return &ResponseOK{"Password updated"}, nil
}

func (svc *Service) UpdatePhoneSend(phone, id string) (*PhoneVerifyResponse, error) {
	if err := isPhoneFormat(phone); err != nil {
		return nil, err
	}

	existingUser, _ := svc.userStore.GetByPhone(phone)
	if existingUser != nil {
		return nil, NewError(403, "user with phone: "+phone+" already exist")
	}

	code := codeGenerator(6)
	token, err := uuid.GenerateUUID()
	if err != nil {
		return nil, err
	}
	err = svc.verificationStore.Set(&PhoneVerification{
		PhoneNumber: phone,
		Code:        code,
		Token:       token,
		Status:      "update_phone_send",
		UserID:      id,
		TTL:         3 * time.Minute,
	})
	if err != nil {
		return nil, err
	}

	err = svc.smsSenderService.Send(&SMS{
		Phone:   phone,
		Message: code,
	})
	if err != nil {
		return nil, err
	}
	return &PhoneVerifyResponse{VerificationToken: token}, nil
}

func (svc *Service) UpdatePhone(token, code string) (*ResponseOK, error) {
	item, err := svc.verificationStore.Get(token)
	if err != nil {
		return nil, err
	}
	if item.Status != "update_phone_send" {
		return nil, ErrInvalidVerificationToken
	}

	if item.Code != code {
		return nil, ErrInvalidCode
	}

	err = svc.verificationStore.Delete(token)
	if err != nil {
		return nil, err
	}

	isVerified := true
	_, err = svc.userStore.Update(&UserUpdate{
		ID:                  item.UserID,
		PhoneNumber:         &item.PhoneNumber,
		PhoneNumberVerified: &isVerified,
	})
	if err != nil {
		return nil, err
	}

	err = svc.verificationStore.Delete(token)
	if err != nil {
		return nil, err
	}
	return &ResponseOK{"Phone Updated"}, nil
}

func (svc *Service) ResetPasswordSend(phone string) (*PhoneVerifyResponse, error) {
	if err := isPhoneFormat(phone); err != nil {
		return nil, err
	}

	existingUser, err := svc.userStore.GetByPhone(phone)
	if err != nil {
		return nil, err
	}

	code := codeGenerator(6)
	token, err := uuid.GenerateUUID()
	if err != nil {
		return nil, err
	}
	err = svc.verificationStore.Set(&PhoneVerification{
		PhoneNumber: phone,
		Code:        code,
		Token:       token,
		Status:      "reset_password_send",
		UserID:      existingUser.ID,
		TTL:         3 * time.Minute,
	})
	if err != nil {
		return nil, err
	}

	err = svc.smsSenderService.Send(&SMS{
		Phone:   phone,
		Message: code,
	})
	if err != nil {
		return nil, err
	}
	return &PhoneVerifyResponse{VerificationToken: token}, nil
}

func (svc *Service) ResetPasswordVerify(token, code string) (*PhoneVerifyResponse, error) {
	item, err := svc.verificationStore.Get(token)
	if err != nil {
		return nil, err
	}
	if item.Status != "reset_password_send" {
		return nil, ErrInvalidVerificationToken
	}

	if item.Code != code {
		return nil, ErrInvalidCode
	}

	err = svc.verificationStore.Set(&PhoneVerification{
		PhoneNumber: item.PhoneNumber,
		Code:        item.Code,
		Token:       item.Token,
		Status:      "reset_password_verified",
		UserID:      item.UserID,
		TTL:         3 * time.Minute,
	})
	if err != nil {
		return nil, err
	}

	return &PhoneVerifyResponse{token}, nil
}

func (svc *Service) ResetPassword(token, password string) (*ResponseOK, error) {
	item, err := svc.verificationStore.Get(token)
	if err != nil {
		return nil, err
	}
	if item.Status != "reset_password_verified" {
		return nil, ErrInvalidVerificationToken
	}

	newPass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	password = string(newPass)

	user := &UserUpdate{
		ID:       item.UserID,
		Password: &password,
	}
	_, err = svc.userStore.Update(user)
	if err != nil {
		return nil, err
	}

	err = svc.verificationStore.Delete(token)
	if err != nil {
		return nil, err
	}

	return &ResponseOK{"Password reset finished"}, nil
}

func (svc *Service) GetDirection(directionReq *DirectionsRequest) (*Direction, error) {
	routes, waypoints, err := svc.mapsService.GetDirection(&maps.DirectionsRequest{
		Origin:      directionReq.Origin,
		Destination: directionReq.Destination,
		Mode:        "driving",
		Language:    "ru",
	})
	if err != nil {
		return nil, err
	}
	cost := float64(routes[0].Legs[0].Distance.Meters) * svc.costPer100Meter
	log.Println("cost for this route:", cost, "KZT")
	return &Direction{
		Routes:            routes,
		GeocodedWaypoints: waypoints,
		Cost:              cost,
	}, nil
}

func (svc *Service) GetAddressInfo(geocodingReq *GeocodingRequest) ([]maps.GeocodingResult, error) {
	return svc.mapsService.GetAddressInfo(&maps.GeocodingRequest{
		Address:  geocodingReq.Address,
		Region:   "kz",
		Language: "ru",
	})
}

func (svc *Service) ListOrders() ([]Order, error) {
	return svc.orderStore.ListByStatus("posted")
}

func (svc *Service) OrderTaxi(orderReq *OrderTaxiRequest, userID string) (*Order, error) {
	direction, err := svc.GetDirection(&DirectionsRequest{
		Origin:      orderReq.Origin,
		Destination: orderReq.Destination,
	})
	if err != nil {
		return nil, err
	}
	order, err := NewOrder()
	if err != nil {
		return nil, err
	}
	order.Origin = orderReq.Origin
	order.Destination = orderReq.Destination
	order.Cost = direction.Cost
	order.PassengerID = userID
	order.PassengerDeviceID = orderReq.FirebaseToken
	order.Status = "posted"
	order.TTL = 15 * time.Minute

	err = svc.orderStore.Set(order)
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (svc *Service) GetOrderUpdates(id string) (*Order, error) {
	return svc.orderStore.Get(id)
}

func (svc *Service) TakeOrder(req *TakeOrderRequest) (*Order, error) {
	order, err := svc.orderStore.Get(req.OrderID)
	if err != nil {
		return nil, err
	}
	if order.Status == "picked" {
		return nil, NewError(403, "already picked")
	}
	order.Status = "picked"
	order.DriverID = req.UserID
	order.DriverDeviceID = req.FirebaseToken
	err = svc.orderStore.Set(order)
	if err != nil {
		return nil, err
	}
	err = svc.firebaseNotificationService.SendNotification(&NotificationRequest{
		To: order.PassengerDeviceID,
		Notification: Notification{
			Title: "Ваш заказ начал обработку",
			Body:  order.DriverID + " начал обработку вашего заказа",
		},
	})
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (svc *Service) PassengerConnection(orderID, key string) (*Order, error) {
	order, err := svc.orderStore.Get(orderID)
	if err != nil {
		return nil, err
	}
	if order.Status != "picked" {
		return nil, NewError(403, "invalid order")
	}
	if order.Key != key {
		return nil, NewError(403, "not your order")
	}
	return order, nil
}

func (svc *Service) DriverConnection(req *OrderConnectionRequest) (*Order, error) {
	order, err := svc.orderStore.Get(req.OrderID)
	if err != nil {
		return nil, err
	}
	if order.Status != "picked" {
		return nil, NewError(403, "invalid order")
	}
	if order.Key != req.Key {
		return nil, NewError(403, "not your order")
	}
	order.DriverLocation = req.DriverLocation
	err = svc.orderStore.Set(order)
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (svc *Service) ReadyToStart(req *OrderConnectionRequest) (*Order, error) {
	order, err := svc.orderStore.Get(req.OrderID)
	if err != nil {
		return nil, err
	}
	if order.Status != "picked" {
		return nil, NewError(403, "invalid order")
	}
	if order.Key != req.Key {
		return nil, NewError(403, "not your order")
	}
	if err = svc.mapsService.IsNear(req.DriverLocation, order.Origin); err != nil {
		return nil, err
	}
	order.Status = "drv_waiting"
	if err = svc.orderStore.Set(order); err != nil {
		return nil, err
	}
	err = svc.firebaseNotificationService.SendNotification(&NotificationRequest{
		To: order.PassengerDeviceID,
		Notification: Notification{
			Title: "Водитель ожидает вас",
			Body:  order.DriverID + " приехал на место",
		},
	})
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (svc *Service) StartRide(req *OrderConnectionRequest) (*Order, error) {
	order, err := svc.orderStore.Get(req.OrderID)
	if err != nil {
		return nil, err
	}
	if order.Status != "drv_waiting" {
		return nil, NewError(403, "invalid order")
	}
	if order.Key != req.Key {
		return nil, NewError(403, "not your order")
	}

	order.Status = "started"
	if err = svc.orderStore.Set(order); err != nil {
		return nil, err
	}
	err = svc.firebaseNotificationService.SendNotification(&NotificationRequest{
		To: order.PassengerDeviceID,
		Notification: Notification{
			Title: "Поездка началась",
			Body:  order.DriverID + " начинает поездку",
		},
	})
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (svc *Service) EndRide(req *OrderConnectionRequest) (*Order, error) {
	order, err := svc.orderStore.Get(req.OrderID)
	if err != nil {
		return nil, err
	}
	if order.Status != "started" {
		return nil, NewError(403, "invalid order")
	}
	if order.Key != req.Key {
		return nil, NewError(403, "not your order")
	}

	if err = svc.mapsService.IsNear(req.DriverLocation, order.Origin); err != nil {
		return nil, err
	}
	order.Status = "ended"
	if err = svc.orderStore.Set(order); err != nil {
		return nil, err
	}
	err = svc.firebaseNotificationService.SendNotification(&NotificationRequest{
		To: order.PassengerDeviceID,
		Notification: Notification{
			Title: "Вы на месте",
			Body:  "Спасибо что выбрали нас",
		},
	})
	if err != nil {
		return nil, err
	}
	return order, nil
}

// TODO
//func (svc *Service) CancelRideByDriver()
//func (svc *Service) CancelRideByPassenger()

func (svc *Service) Welcome(username string) (interface{}, error) {
	return struct {
		Message string `json:"message"`
	}{"Welcome, " + username}, nil
}

func (svc *Service) List() ([]User, error) {
	return nil, nil
	//return srv.UserStore.List()
}

const letterBytes = "0123456789"

func codeGenerator(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func isPhoneFormat(phone string) error {
	if len(phone) != 12 && phone[0] != '+' {
		return ErrWrongPhoneFormat
	}
	return nil
}
