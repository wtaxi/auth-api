package auth

import (
	"encoding/json"
	"github.com/go-redis/redis"
	"strconv"
)

type RedisConfig struct {
	Host     string
	Password string
	Port     int
	DB       int
}

type VerificationStore struct {
	cfg RedisConfig
	clt *redis.Client
}

func NewVerificationStore(cfg RedisConfig) (*VerificationStore, error) {
	strconv.Itoa(cfg.DB)

	client := redis.NewClient(&redis.Options{
		Addr:     cfg.Host + ":" + strconv.Itoa(cfg.Port),
		Password: cfg.Password, // no password set
		DB:       cfg.DB,       // use default DB
	})

	_, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}
	return &VerificationStore{cfg, client}, nil
}

func (store *VerificationStore) Get(token string) (*PhoneVerification, error) {
	result, err := store.clt.Get("phone_verification:"+token).Bytes()
	if err != nil {
		return nil, err
	}
	item := &PhoneVerification{}
	if err := json.Unmarshal(result, item); err != nil {
		return nil, err
	}
	return item, nil
}

func (store *VerificationStore) Set(item *PhoneVerification) error {
	data, err := json.Marshal(item)
	if err != nil {
		return err
	}
	_, err = store.clt.Set("phone_verification:"+item.Token, data, item.TTL).Result()
	if err != nil {
		return err
	}
	return nil
}

func (store *VerificationStore) Delete(token string) error {
	_, err := store.clt.Del("phone_verification:"+token).Result()
	return err
}
