package auth

import "errors"

var (
	ErrUserNotFound             = Error{404, errors.New("user not found")}
	ErrNothingToUpdate          = Error{400, errors.New("nothing to update")}
	ErrPasswordNotProvided      = errors.New("password not provided")
	ErrUsernameNotProvided      = errors.New("phone number not provided")
	ErrInvalidCode              = Error{403, errors.New("invalid code")}
	ErrInvalidVerificationToken = Error{403, errors.New("invalid verification token")}
	ErrPhoneNotProvided         = Error{400, errors.New("phone not provided")}
	ErrWrongPhoneFormat         = Error{400, errors.New("wrong phone format")}
)

type Error struct {
	Code int
	Err  error
}

func NewError(code int, msg string) Error {
	return Error{
		Code: code,
		Err:  errors.New(msg),
	}
}

func (se Error) Error() string {
	return se.Err.Error()
}

func (se Error) Status() int {
	return se.Code
}
