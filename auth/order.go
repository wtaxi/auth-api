package auth

import (
	"github.com/hashicorp/go-uuid"
	"time"
)

type Order struct {
	ID                string        `json:"id,omitempty"`
	Key               string        `json:"key,omitempty"`
	Origin            string        `json:"origin,omitempty"`
	Destination       string        `json:"destination,omitempty"`
	Cost              float64       `json:"cost,omitempty"`
	PassengerID       string        `json:"psg_id,omitempty"`
	PassengerDeviceID string        `json:"psg_device_id,omitempty"`
	Status            string        `json:"status,omitempty"`
	DriverID          string        `json:"drv_id,omitempty"`
	DriverDeviceID    string        `json:"drv_device_id,omitempty"`
	DriverLocation    string        `json:"drv_location,omitempty"`
	TTL               time.Duration `json:"ttl,omitempty"`
}

// status: posted, picked, drv_waiting, cancel_by_psg, cancel_by_drv, started, ended

func NewOrder() (*Order, error) {
	id, err := uuid.GenerateUUID()
	if err != nil {
		return nil, err
	}
	key, err := uuid.GenerateUUID()
	if err != nil {
		return nil, err
	}
	return &Order{
		ID: id,
		Key: key,
	}, nil
}
