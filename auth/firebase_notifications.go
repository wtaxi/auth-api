package auth

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type FirebaseNotificationService struct {
	Clt         http.Client
	AuthKey     string

	FirebaseURL string // https://fcm.googleapis.com/fcm/send
}

type NotificationRequest struct {
	To              string       `json:"to,omitempty"`
	RegistrationIds string       `json:"registration_ids,omitempty"`
	TTL             int          `json:"time_to_live,omitempty"`
	Notification    Notification `json:"notification,omitempty"`
}

type Notification struct {
	Title       string `json:"title,omitempty"`
	Body        string `json:"body,omitempty"`
	ClickAction string `json:"click_action,omitempty"`
}

func (fns *FirebaseNotificationService) SendNotification(req *NotificationRequest) error {
	jsonBody, err := json.Marshal(req)
	if err != nil {
		return err
	}
	request, err := http.NewRequest("POST", fns.FirebaseURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return err
	}
	request.Header.Set("Authorization", "key="+fns.AuthKey)
	request.Header.Set("Content-Type", "application/json")

	resp, err := fns.Clt.Do(request)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return NewError(500, "something went wrong")
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	log.Println(string(respBody))
	return nil
}
