package auth

import (
	"fmt"
	"gitlab.com/wtaxi/auth-api/helper"
	"net/http"
	"time"
)

type HttpEndpointFactory struct {
	service *Service
}

func NewHttpEndpointFactory(svc *Service) *HttpEndpointFactory {
	return &HttpEndpointFactory{service: svc}
}

func (fac *HttpEndpointFactory) ListEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	return nil, nil
}

func (fac *HttpEndpointFactory) TokenEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	cmd := &TokenRequest{}
	if err := helper.UnmarshalJSON(r, cmd); err != nil {
		return nil, err
	}
	if cmd.GrantType == "refresh" {
		cmd.Claims = r.Context().Value("props").(*helper.Claims)
	}
	resp, err := fac.service.Token(cmd)
	if err != nil {
		return nil, err
	}

	return &helper.Response{
		Body:       resp,
		StatusCode: 200,
	}, nil
}

func (fac *HttpEndpointFactory) WelcomeEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	claims := r.Context().Value("props").(*helper.Claims)
	resp, err := fac.service.Welcome(claims.PhoneNumber)
	if err != nil {
		return nil, err
	}

	return &helper.Response{
		Body:       resp,
		StatusCode: 200,
	}, nil
}

func (fac *HttpEndpointFactory) SignUpEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	vars := r.URL.Query()
	response := &helper.Response{
		StatusCode: 200,
	}
	if r.Method == "GET" {
		phone := vars.Get("phone")
		if phone == "" {
			return nil, Error{400, ErrPhoneNotProvided}
		}

		resp, err := fac.service.SignUpSend(phone)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else if r.Method == "POST" {
		code := vars.Get("code")
		token := vars.Get("verification_token")
		if code == "" {
			return nil, ErrInvalidCode
		}
		if token == "" {
			return nil, ErrInvalidVerificationToken
		}
		resp, err := fac.service.SignUpVerify(token, code)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else if r.Method == "PUT" {
		token := vars.Get("verification_token")
		if token == "" {
			return nil, ErrInvalidVerificationToken
		}
		signUp := &SignUpRequest{}
		if err := helper.UnmarshalJSON(r, signUp); err != nil {
			return nil, err
		}
		resp, err := fac.service.SignUp(token, signUp.Password)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else {
		return nil, NewError(405, "Method not allowed")
	}
	return response, nil
}

func (fac *HttpEndpointFactory) GetDirectionEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	req := &DirectionsRequest{}
	req.Origin = r.URL.Query().Get("origin")
	req.Destination = r.URL.Query().Get("destination")
	resp, err := fac.service.GetDirection(req)
	if err != nil {
		return nil, err
	}

	return &helper.Response{
		Body:       resp,
		StatusCode: 200,
	}, nil
}

func (fac *HttpEndpointFactory) GetAddressInfoEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	req := &GeocodingRequest{}
	req.Address = r.URL.Query().Get("address")
	resp, err := fac.service.GetAddressInfo(req)
	if err != nil {
		return nil, err
	}

	return &helper.Response{
		Body:       resp,
		StatusCode: 200,
	}, nil
}

func (fac *HttpEndpointFactory) OrderTaxiEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	claims := r.Context().Value("props").(*helper.Claims)
	req := &OrderTaxiRequest{}
	if err := helper.UnmarshalJSON(r, req); err != nil {
		return nil, err
	}
	resp, err := fac.service.OrderTaxi(req, claims.Subject)
	if err != nil {
		return nil, err
	}

	return &helper.Response{
		Body:       resp,
		StatusCode: 200,
	}, nil
}

//func (fac *HttpEndpointFactory) GetOrderUpdatesEndpoint(w http.ResponseWriter, r *http.Request) {
//	id := r.URL.Query().Get("order_id")
//	ws, err := helper.Upgrade(w, r)
//	if err != nil {
//		fmt.Fprint(w, err)
//	}
//
//	go helper.WebSocketMDW(ws, func(messageType int, body interface{}) (interface{}, error) {
//		return fac.service.GetOrderUpdates(id)
//	})
//}

func (fac *HttpEndpointFactory) DriverOrderEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	response := &helper.Response{
		StatusCode: 200,
	}
	claims := r.Context().Value("props").(*helper.Claims)
	if r.Method == "POST" {
		req := &TakeOrderRequest{}
		if err := helper.UnmarshalJSON(r, req); err != nil {
			return nil, err
		}
		//check user is it driver
		req.UserID = claims.Subject
		resp, err := fac.service.TakeOrder(req)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else if r.Method == "GET" {
		//check user is it driver
		resp, err := fac.service.ListOrders()
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else {
		return nil, NewError(405, "Method not allowed")
	}
	return response, nil
}

func (fac *HttpEndpointFactory) UpdatePasswordEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	claims := r.Context().Value("props").(*helper.Claims)
	req := &UpdatePasswordRequest{}
	if err := helper.UnmarshalJSON(r, req); err != nil {
		return nil, err
	}
	req.UserID = claims.Subject
	resp, err := fac.service.UpdatePassword(req)
	if err != nil {
		return nil, err
	}
	return &helper.Response{
		Body:       resp,
		StatusCode: 200,
	}, nil
}

func (fac *HttpEndpointFactory) UpdatePhoneEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	vars := r.URL.Query()
	response := &helper.Response{
		StatusCode: 200,
	}
	req := &UpdatePhoneRequest{}
	if r.Method == "GET" {
		claims := r.Context().Value("props").(*helper.Claims)
		phone := vars.Get("phone")
		if phone == "" {
			return nil, Error{400, ErrPhoneNotProvided}
		}
		resp, err := fac.service.UpdatePhoneSend(phone, claims.Subject)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else if r.Method == "POST" {
		if err := helper.UnmarshalJSON(r, req); err != nil {
			return nil, err
		}
		resp, err := fac.service.UpdatePhone(req.VerificationToken, req.Code)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else {
		return nil, NewError(405, "Method not allowed")
	}
	return response, nil
}

func (fac *HttpEndpointFactory) ResetPasswordEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	vars := r.URL.Query()
	response := &helper.Response{
		StatusCode: 200,
	}
	if r.Method == "GET" {
		phone := vars.Get("phone")
		if phone == "" {
			return nil, Error{400, ErrPhoneNotProvided}
		}

		resp, err := fac.service.ResetPasswordSend(phone)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else if r.Method == "POST" {
		code := vars.Get("code")
		token := vars.Get("verification_token")
		if code == "" {
			return nil, ErrInvalidCode
		}
		if token == "" {
			return nil, ErrInvalidVerificationToken
		}
		resp, err := fac.service.ResetPasswordVerify(token, code)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else if r.Method == "PUT" {
		token := vars.Get("verification_token")
		if token == "" {
			return nil, ErrInvalidVerificationToken
		}
		signUp := &SignUpRequest{}
		if err := helper.UnmarshalJSON(r, signUp); err != nil {
			return nil, err
		}
		resp, err := fac.service.ResetPassword(token, signUp.Password)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else {
		return nil, NewError(405, "Method not allowed")
	}
	return response, nil
}

func (fac *HttpEndpointFactory) ListOrdersEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	//check is it driver
	//claims := r.Context().Value("props").(*helper.Claims)
	resp, err := fac.service.ListOrders()
	if err != nil {
		return nil, err
	}
	return &helper.Response{
		Body: resp,
		StatusCode: 200,
	}, nil
}

// websocket endpoint
func (fac *HttpEndpointFactory) PassengerConnectionEndpoint(w http.ResponseWriter, r *http.Request) {
	//check is it passenger
	//claims := r.Context().Value("props").(*helper.Claims)
	ws, err := helper.Upgrade(w, r)
	if err != nil {
		fmt.Fprint(w, err)
	}

	defer ws.Close()

	cmd := &OrderConnectionRequest{}
	go helper.WebSocketMDW(ws, time.Second, cmd, func(v interface{}) (interface{}, error) {
		cmd, ok := v.(*OrderConnectionRequest)
		if !ok {
			return nil, err
		}
		return fac.service.PassengerConnection(cmd.OrderID, cmd.Key)
	})
}

// websocket endpoint
func (fac *HttpEndpointFactory) DriverConnectionEndpoint(w http.ResponseWriter, r *http.Request) {
	//check is it driver
	//claims := r.Context().Value("props").(*helper.Claims)
	ws, err := helper.Upgrade(w, r)
	if err != nil {
		fmt.Fprint(w, err)
	}

	defer ws.Close()
	cmd := &OrderConnectionRequest{}
	go helper.WebSocketMDW(ws, time.Second, cmd, func(v interface{}) (interface{}, error) {
		cmd, ok := v.(*OrderConnectionRequest)
		if !ok {
			return nil, err
		}
		return fac.service.DriverConnection(cmd)
	})
}

func (fac *HttpEndpointFactory) ReadyToStartEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	//check is it driver
	//claims := r.Context().Value("props").(*helper.Claims)
	cmd := &OrderConnectionRequest{}
	if err := helper.UnmarshalJSON(r, cmd); err != nil {
		return nil, err
	}
	resp, err := fac.service.ReadyToStart(cmd)
	if err != nil {
		return nil, err
	}
	return &helper.Response{
		Body: resp,
		StatusCode: 200,
	}, nil
}

func (fac *HttpEndpointFactory) StartRideEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	//check is it driver
	//claims := r.Context().Value("props").(*helper.Claims)
	cmd := &OrderConnectionRequest{}
	if err := helper.UnmarshalJSON(r, cmd); err != nil {
		return nil, err
	}
	resp, err := fac.service.StartRide(cmd)
	if err != nil {
		return nil, err
	}
	return &helper.Response{
		Body: resp,
		StatusCode: 200,
	}, nil
}

func (fac *HttpEndpointFactory) EndRideEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	//check is it driver
	//claims := r.Context().Value("props").(*helper.Claims)
	cmd := &OrderConnectionRequest{}
	if err := helper.UnmarshalJSON(r, cmd); err != nil {
		return nil, err
	}
	resp, err := fac.service.EndRide(cmd)
	if err != nil {
		return nil, err
	}
	return &helper.Response{
		Body: resp,
		StatusCode: 200,
	}, nil
}