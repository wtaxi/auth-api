package auth

import (
	"gitlab.com/wtaxi/auth-api/helper"
	"googlemaps.github.io/maps"
	"time"
)

type User struct {
	ID                  string `json:"id,omitempty"`
	Password            string `json:"password,omitempty"`
	PhoneNumber         string `json:"phone_number,omitempty"`
	PhoneNumberVerified bool   `json:"phone_number_verified,omitempty"`
	FirstName           string `json:"first_name,omitempty"`
	LastName            string `json:"last_name,omitempty"`
	Gender              string `json:"gender,omitempty"`
	DateOfBirth         string `json:"date_of_birth,omitempty"`
}

type UserUpdate struct {
	ID                  string  `json:"id,omitempty"`
	Password            *string `json:"password,omitempty"`
	PhoneNumber         *string `json:"phone_number,omitempty"`
	PhoneNumberVerified *bool   `json:"phone_number_verified,omitempty"`
	FirstName           *string `json:"first_name,omitempty"`
	LastName            *string `json:"last_name,omitempty"`
	Gender              *string `json:"gender,omitempty"`
	DateOfBirth         *string `json:"date_of_birth,omitempty"`
}

type PhoneVerification struct {
	PhoneNumber string        `json:"phone_number"`
	Code        string        `json:"code"`
	Token       string        `json:"token"`
	Status      string        `json:"status"`
	UserID      string        `json:"user_id,omitempty"`
	TTL         time.Duration `json:"ttl"`
}

type TokenRequest struct {
	GrantType    string         `json:"grant_type,omitempty"`
	PhoneNumber  string         `json:"phone_number,omitempty"`
	Password     string         `json:"password,omitempty"`
	RefreshToken string         `json:"refresh_token,omitempty"`
	Claims       *helper.Claims `json:"-"`
}

type TokenResponse struct {
	Token string `json:"token"`
}

type SignUpRequest struct {
	Password string `json:"password"`
}

type SMS struct {
	Phone   string `json:"phone"`
	Message string `json:"message"`
}

type PhoneVerifyResponse struct {
	VerificationToken string `json:"verification_token"`
}

type ResponseOK struct {
	Message string `json:"message"`
}

type Direction struct {
	Routes            []maps.Route            `json:"routes,omitempty"`
	GeocodedWaypoints []maps.GeocodedWaypoint `json:"geocoded_waypoints,omitempty"`
	Cost              float64                 `json:"cost,omitempty"`
}

type DirectionsRequest struct {
	Origin      string `json:"origin,omitempty"`
	Destination string `json:"destination,omitempty"`
}

type GeocodingRequest struct {
	Address string `json:"address,omitempty"`
}

type OrderTaxiRequest struct {
	Origin        string `json:"origin,omitempty"`
	Destination   string `json:"destination,omitempty"`
	FirebaseToken string `json:"firebase_token,omitempty"`
}

type TakeOrderRequest struct {
	UserID        string `json:"-"`
	OrderID       string `json:"order_id,omitempty"`
	FirebaseToken string `json:"firebase_token,omitempty"`
}

type UpdatePasswordRequest struct {
	UserID      string `json:"-"`
	NewPassword string `json:"password"`
}

type UpdatePhoneRequest struct {
	UserID            string `json:"-"`
	VerificationToken string `json:"verification_token,omitempty"`
	Code              string `json:"code,omitempty"`
}

type OrderConnectionRequest struct {
	OrderID        string `json:"order_id"`
	Key            string `json:"key"`
	DriverLocation string `json:"location,omitempty"`
}