package auth

import (
	"encoding/json"
	"github.com/go-redis/redis"
	"strconv"
)

type OrderStore struct {
	cfg RedisConfig
	clt *redis.Client
}

func NewOrderStore(cfg RedisConfig) (*OrderStore, error) {
	strconv.Itoa(cfg.DB)

	client := redis.NewClient(&redis.Options{
		Addr:     cfg.Host + ":" + strconv.Itoa(cfg.Port),
		Password: cfg.Password, // no password set
		DB:       cfg.DB,       // use default DB
	})

	_, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}
	return &OrderStore{cfg, client}, nil
}

func (store *OrderStore) List() ([]Order, error) {
	keys, err := store.clt.Keys("order/").Result()
	if err != nil {
		return nil, err
	}

	if len(keys) == 0 { keys = []string{""} }

	result, err := store.clt.MGet(keys...).Result()
	if err != nil {
		return nil, err
	}
	items := []Order{}
	for _, item := range result {
		order := Order{}
		if err := json.Unmarshal(item.([]byte), &order); err != nil {
			return nil, err
		}
		order.Key = ""
		items = append(items, order)
	}
	return items, nil
}

func (store *OrderStore) ListByStatus(status string) ([]Order, error) {
	keys, err := store.clt.Keys("order/").Result()
	if err != nil {
		return nil, err
	}

	if len(keys) == 0 { keys = []string{""} }

	result, err := store.clt.MGet(keys...).Result()
	if err != nil {
		return nil, err
	}
	items := []Order{}
	for _, item := range result {
		order := Order{}
		if err := json.Unmarshal(item.([]byte), &order); err != nil {
			return nil, err
		}
		order.Key = ""
		if order.Status == status {
			items = append(items, order)
		}
	}
	return items, nil
}

func (store *OrderStore) Get(id string) (*Order, error) {
	result, err := store.clt.Get("order/"+id).Bytes()
	if err != nil {
		return nil, err
	}
	item := &Order{}
	if err := json.Unmarshal(result, item); err != nil {
		return nil, err
	}
	return item, nil
}

func (store *OrderStore) Set(item *Order) error {
	data, err := json.Marshal(item)
	if err != nil {
		return err
	}
	_, err = store.clt.Set("order/"+item.ID, data, item.TTL).Result()
	if err != nil {
		return err
	}
	return nil
}

func (store *OrderStore) Delete(id string) error {
	_, err := store.clt.Del("order/"+id).Result()
	return err
}
