FROM golang:1.14.5-alpine AS builder

RUN apk update && apk add --no-cache git
RUN echo "[url \"git@gitlab.com:\"]\n\tinsteadOf = https://gitlab.com/" >> /root/.gitconfig
RUN mkdir /root/.ssh && echo "StrictHostKeyChecking no " > /root/.ssh/config

ADD . /go/src/gitlab.com/wtaxi/auth-api
WORKDIR $GOPATH/src/gitlab.com/wtaxi/auth-api

CMD go get -d -v && go mod download && go mod verify && go run main.go
